package servon.dao;

import java.util.Date;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import servon.entity.UserEntity;
import servon.mapper.UserRowMapper;

@Repository("uDaoImplRepo")
public class UserDaoImplRepo implements UserDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public int insertedRecords;
	
	final static Logger log= LogManager.getLogger(UserDaoImplRepo.class);
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplateFromTest){
		jdbcTemplate = jdbcTemplateFromTest;
	}

	@Override
	public List<UserEntity> getAllUserDetails() {
		List<UserEntity> query = null;
		final String RETRIEVINGALLUSERSSQL ="select * from USERS";
		try {
			log.info(new Date() +"executing the SQL statement");
		query = jdbcTemplate.query(RETRIEVINGALLUSERSSQL, new UserRowMapper());
		
		}catch(Exception e) {
			log.error(new Date() + "exception thown");
		}
		return query;
	}

	@Override
	public UserEntity getUserById(String userName) {
		final String RETRIEVINGUSERBYIDSQL = "select USERNAME, PASSWORD, EMAIL_ID from USERS where USERNAME = ?";
		UserEntity queryForObject = jdbcTemplate.queryForObject(RETRIEVINGUSERBYIDSQL, new UserRowMapper(), userName);
		return queryForObject;
	}

	@Override
	public void insertValuesInUsers(UserEntity userEntity) {
		final String INSERTQUERYSQL ="INSERT INTO USERS (USERNAME, PASSWORD, EMAIL_ID) values(?,?,?)";
		String userName = userEntity.getUserName();
		String password = userEntity.getPassword();
		String email = userEntity.getEmailId();
		insertedRecords = jdbcTemplate.update(INSERTQUERYSQL, new Object[]{userName,password,email});
	}

	@Override
	public void updateValuesInUsers(UserEntity userEntity) {
		final String UPDATEQUERYSQL = "UPDATE USERS SET PASSWORD=?, EMAIL_ID=? WHERE USERNAME =?";
		String userName = userEntity.getUserName();
		String password = userEntity.getPassword();
		String email = userEntity.getEmailId();
		jdbcTemplate.update(UPDATEQUERYSQL, new Object[] {password,email,userName});
	}
	
	

}
