package servon.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(" servon.dao")
@EntityScan("servon.entity")
@ComponentScan(basePackages= {"servon.controller","servon.service"," servon.dao", "servon.mapper"})
public class ServonMtMicroApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServonMtMicroApplication.class, args);
	}
}
