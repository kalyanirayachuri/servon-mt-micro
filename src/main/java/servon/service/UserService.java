package servon.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import servon.dao.UserDao;
import servon.entity.UserEntity;

@Service
public class UserService {
	@Autowired
	@Qualifier("uDaoImplRepo")
	private UserDao userDao;
	
	public List<UserEntity> getAllUserDetails() {
		return userDao.getAllUserDetails();
		
	}
	public UserEntity getUserById(String userName) {
		UserEntity userById = userDao.getUserById(userName);
		return userById;
		
	}
	public void insertValuesInUsers(UserEntity userEntity) {
		userDao.insertValuesInUsers(userEntity);
	}
	
	public void updateValuesInUsers(UserEntity userEntity) {
		userDao.updateValuesInUsers(userEntity);
	}
}
