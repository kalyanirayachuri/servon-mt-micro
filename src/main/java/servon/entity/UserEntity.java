package servon.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UserEntity {
	/*@GeneratedValue(strategy =GenerationType.AUTO)
	private int userId;*/
	private String userName;
	private String password;
	
	@Id
	//@Column(name = "EMAIL_ID", nullable = false)
	private String emailId;
	/*public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}*/
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
}
