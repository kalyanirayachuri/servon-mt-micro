package servon.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import servon.entity.UserEntity;

@Repository
public interface UserDao {
	public List<UserEntity> getAllUserDetails();
	public UserEntity getUserById(String userName);
	public void insertValuesInUsers(UserEntity userEntity);
	public void updateValuesInUsers(UserEntity userEntity);
}
