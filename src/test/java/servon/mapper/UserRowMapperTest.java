package servon.mapper;

import static org.junit.Assert.assertNotNull;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import servon.entity.UserEntity;

public class UserRowMapperTest {

	UserRowMapper userRowMapper;
	ResultSet resultSet;
	
	@Before
	public void setUp() throws Exception {
		resultSet = EasyMock.createMock(ResultSet.class);
		userRowMapper = new UserRowMapper();
	}

	@After
	public void tearDown() throws Exception {
		resultSet = null;
		userRowMapper = null;
	}

	@Ignore
	@Test
	public void testMapRow() throws SQLException {
		
		EasyMock.expect(resultSet.getString(EasyMock.anyObject(String.class))).andReturn("SAMPLE").anyTimes();
		
		EasyMock.replay(resultSet);
		
		UserEntity userEntity = userRowMapper.mapRow(resultSet,EasyMock.anyInt());
		
		EasyMock.verify(resultSet);
		
		assertNotNull(userEntity);
		
		
	}

}
