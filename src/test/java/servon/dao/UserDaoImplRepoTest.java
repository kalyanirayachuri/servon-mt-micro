package servon.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import servon.entity.UserEntity;
import servon.mapper.UserRowMapper;

public class UserDaoImplRepoTest {
	
	UserDaoImplRepo userDaoImplRepo;
	
	JdbcTemplate jdbcTemplateMock;
	
	UserEntity userEntityMock;
	
	@Before
	public void setUp() throws Exception {
		userDaoImplRepo = new UserDaoImplRepo();
		jdbcTemplateMock = EasyMock.createMock(JdbcTemplate.class);
		userEntityMock = EasyMock.createMock(UserEntity.class);
		
		userDaoImplRepo.setJdbcTemplate(jdbcTemplateMock);
		
	}

	@After
	public void tearDown() throws Exception {
		userDaoImplRepo = null;
		jdbcTemplateMock = null;
	}

	@Test
	public void testGetAllUserDetails() {
		EasyMock.expect(jdbcTemplateMock.query(EasyMock.anyObject(String.class), EasyMock.<RowMapper<UserEntity>> anyObject())).andReturn(new ArrayList<UserEntity>());
		
		EasyMock.replay(jdbcTemplateMock);
		
		List<UserEntity> allUserDetails = userDaoImplRepo.getAllUserDetails();
		
		EasyMock.verify(jdbcTemplateMock);
		
		assertNotNull(allUserDetails);
	}

	@Test
	public void testGetUserById() {
		
		/*EasyMock.expect( jdbcTemplateMock.queryForObject(EasyMock.anyObject(String.class), EasyMock.anyObject(UserRowMapper.class),userName));*/
		EasyMock.expect(jdbcTemplateMock.queryForObject(EasyMock.anyObject(String.class), EasyMock.anyObject(UserRowMapper.class),EasyMock.anyObject(String.class))).andReturn(new UserEntity()).once();
		
		
		EasyMock.replay(jdbcTemplateMock);
		UserEntity user = userDaoImplRepo.getUserById(EasyMock.anyObject(String.class));	
		EasyMock.verify(jdbcTemplateMock);
		
	}
	
	/*@Test
	public void testInsertValuesInUsers() {
		EasyMock.expect(userEntityMock.getUserName()).andReturn("UserName Mock");
		EasyMock.expect(userEntityMock.getPassword()).andReturn("Password Mock");
		EasyMock.expect(userEntityMock.getEmailId()).andReturn("EmailId Mock");
		
		EasyMock.expect(jdbcTemplateMock.update(EasyMock.anyObject(String.class), EasyMock.<Object[]> anyObject())).andReturn(new Integer(1)).anyTimes();
		
		EasyMock.replay(userEntityMock, jdbcTemplateMock);
		
		userDaoImplRepo.insertValuesInUsers(userEntityMock);
		
		EasyMock.verify(userEntityMock, jdbcTemplateMock);
		
		System.out.println(userDaoImplRepo.insertedRecords);
	}*/
}
