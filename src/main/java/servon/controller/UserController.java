package servon.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import servon.entity.UserEntity;
import servon.service.UserService;

@RestController
public class UserController {
	@Autowired
	private UserService userService;

	@RequestMapping("/users")
	public List<UserEntity> getAllUserDetails(){
		return userService.getAllUserDetails();
	}
	
	/*@RequestMapping(method = RequestMethod.GET, value = "/users/userName")*/
	@GetMapping("/users/{userName}")
	public UserEntity getUserById(@PathVariable String userName) {
		return userService.getUserById(userName);
	}
	
	@PostMapping("/userEntity")
	public void insertValuesInUsers(@RequestBody UserEntity userEntity) {
		userService.insertValuesInUsers(userEntity);
	}
	
	@PutMapping("/userEntity")
	public void updateValuesInUsers(@RequestBody UserEntity userEntity) {
		userService.updateValuesInUsers(userEntity);
	}
}
